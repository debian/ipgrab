/**************************************************************************** 
** File: ipxrip.c
**
** Author: Mike Borella
**
** Dump IPX/RIP header information
**
** $Id: ipxrip.c,v 1.8 2002/01/03 00:04:01 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "ipxrip.h"

#define HOLDER_SIZE 64

strmap_t ipxrip_command_map [] =
{
  { IPXRIP_COMMAND_REQUEST,   "request" },
  { IPXRIP_COMMAND_RESPONSE,  "response" },
  { 0, "" }
};

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_ipxrip()
**
** Parse IPX/RIP header and dump fields
**
**----------------------------------------------------------------------------
*/

void dump_ipxrip(packet_t *pkt)
{
  ipxrip_header_t  rip;
  ipxrip_entry_t   e;
  char holder[HOLDER_SIZE];
  u_int8_t count = 0;
 
  /* Set the layer */
  set_layer(LAYER_NETWORK);

  /*
   * Get the IPX RIP header
   */

  if (get_packet_bytes((u_int8_t *) &rip, pkt, sizeof(ipxrip_header_t)) 
		       == 0)
    return;

  /*
   * Conversions
   */

  rip.op = ntohs(rip.op);

  /*
   * Dump header
   */

  if (my_args->m)
    {
      /* what to do with minimal mode here?? */
      return;
    }
  else
    {
      /* announcement */
      display_header_banner("IPX RIP Header");
      
      /* operation */
      snprintf(holder, HOLDER_SIZE, "%d (%s)", rip.op, 
	       map2str(ipxrip_command_map, rip.op));
      display("Operation", (u_int8_t *) holder, strlen(holder), DISP_STRING);
      
      /*
       * Loop forever dumping routing entries.  Get_packet_bytes() failing
       * kicks us out when the time comes.
       */
      
      while(1)
	{
	  /* Get the entry */
	  if (get_packet_bytes((u_int8_t *) &e, pkt, sizeof(ipxrip_entry_t)) 
	      == 0)
	    return;
	  
	  /* increment count */
	  count++;
	  
	  /* do conversions */
	  e.net = htonl(e.net);
	  e.hops = htons(e.hops);
	  e.ticks = htons(e.ticks);
	  
	  /* Show the route */
	  display("Route", (u_int8_t *) &count, 1, DISP_DEC);
	  display("  Network", (u_int8_t *) &e.net, 4, DISP_HEX);
	  display("  Hops", (u_int8_t *) &e.hops, 2, DISP_DEC);
	  display("  Ticks", (u_int8_t *) &e.ticks, 2, DISP_DEC);
	}
    } /* else */
  
}


