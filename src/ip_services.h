/**************************************************************************** 
** File: ip_services.h
**
** Author: Mike Borella
**
** Comments: IP service numbers
**
** $Id: ip_services.h,v 1.12 2006/11/21 07:47:34 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef IP_SERVICES_H
#define IP_SERVICES_H

#include "global.h"
#include "dhcp.h"
#include "dns.h"
#include "http.h"
#include "isakmp.h"
#include "l2tp.h"
#include "mgcp.h"
#include "mobileip.h"
#include "netbios_ns.h"
#include "nntp.h"
#include "pptp.h"
#include "radius.h"
#include "rip.h"
#include "ripng.h"
#include "rtcp.h"
#include "rtp.h"
#include "sip.h"
#include "slp.h"
#include "ssh.h"
#include "ipgsnmp.h"
#include "ftpctrl.h"
#include "tftp.h"

/*
 * List of well-known and non-so-well-known port numbers - NOT DONE YET!
 */

#define PORT_TCPMUX            1
#define PORT_ECHO              7
#define PORT_DISCARD           9
#define PORT_SYSTAT            11
#define PORT_DAYTIME           13
#define PORT_NETSTAT           15
#define PORT_QOTD              17
#define PORT_MSP               18
#define PORT_CHARGEN           19
#define PORT_FTPDATA           20
#define PORT_FTPCTRL           21
#define PORT_SSH               22
#define PORT_TELNET            23
#define PORT_SMTP              25
#define PORT_TIME              37
#define PORT_RLP               39
#define PORT_NAMESERVER        42
#define PORT_WHOIS             43
#define PORT_REMOTEMAILCHECK   50
#define PORT_DNS               53
#define PORT_MTP               57
#define PORT_DHCPSERVER        67
#define PORT_DHCPCLIENT        68
#define PORT_TFTP              69
#define PORT_GOPHER            70
#define PORT_RJE               77
#define PORT_FINGER            79
#define PORT_HTTP              80
#define PORT_TTYLINK           87
#define PORT_KERBEROS          88
#define PORT_SUPDUP            95
#define PORT_HOSTNAMES         101
#define PORT_ISOTSAP           102
#define PORT_CSNETNAMESERVICE  105
#define PORT_EUDORA            106
#define PORT_RTELNET           107
#define PORT_POP2              109
#define PORT_POP3              110
#define PORT_SUNRPC            111
#define PORT_AUTH              113
#define PORT_SFTP              115
#define PORT_UUCPPATH          117
#define PORT_NNTP              119
#define PORT_NETBIOSNS         137
#define PORT_NETBIOSDGM        138
#define PORT_NETBIOSSSN        139
#define PORT_IMAP2             143
#define PORT_SNMP              161
#define PORT_SNMPTRAP          162
#define PORT_CMIPMAN           163
#define PORT_CMIPAGENT         164
#define PORT_XDMCP             177
#define PORT_NEXTSTEP          178
#define PORT_BGP               179
#define PORT_PROSPERO          191
#define PORT_IRC               194
#define PORT_SMUX              199
#define PORT_APPLETALKRTMP     201
#define PORT_APPLETALKNBP      202
#define PORT_APPLETALKECHO     204
#define PORT_APPLETALKZIS      206
#define PORT_QMTP              209
#define PORT_Z3950             210
#define PORT_IPX               213
#define PORT_IMAP3             220
#define PORT_RPC2PORTMAP       369
#define PORT_CODAAUTH2         370
#define PORT_UNIXLISTSERV      372
#define PORT_SLP               427
#define PORT_MOBILEIP          434
#define PORT_HTTPS             443
#define PORT_SNPP              444
#define PORT_SAFT              487
#define PORT_ISAKMP            500
#define PORT_EXECBIFF          512
#define PORT_LOGIN             513
#define PORT_WHO               514
#define PORT_PRINTER           515
#define PORT_TALK              517
#define PORT_NTALK             518
#define PORT_RIP               520
#define PORT_RIPNG             521
#define PORT_TIMED             525
#define PORT_TEMPO             526
#define PORT_COURIER           530
#define PORT_CONFERENCE        531
#define PORT_NETNEWS           532
#define PORT_NETWALL           533
#define PORT_UUCP              540
#define PORT_KLOGIN            543
#define PORT_KSHELL            544
#define PORT_AFPOVERTCP        548
#define PORT_REMOTEFS          556
#define PORT_NPMPLOCAL         610
#define PORT_NPMPGUI           611
#define PORT_HMMPIND           612
#define PORT_CDMA2000A11       699
#define PORT_KERBEROSADM       749
#define PORT_WEBSTER           765

#define PORT_INGRESLOCK        1524
#define PORT_PROSPERONP        1525
#define PORT_DATAMETRICSRADIUS 1645
#define PORT_SAMSG             1646
#define PORT_L2TP              1701
#define PORT_PPTP              1723
#define PORT_RADIUS            1812
#define PORT_RADACCOUNT        1813
#define PORT_CVSSERVER         2401
#define PORT_VENUS             2430
#define PORT_VENUSSE           2431
#define PORT_CODASERVER        2432
#define PORT_CODASERVERSE      2433
#define PORT_MYSQL             3306
#define PORT_RFE               5002
#define PORT_SIP               5060
#define PORT_CFENGINE          5308
#define PORT_BBS               7000 

/*
 * Define a type that is a function that takes a packet_t pointer and 
 * returns a void 
 */

typedef void (*service_func_t)(packet_t *);

/*
 * This function maps a port number to a function that processes the 
 * protocol that runs on that port
 */

service_func_t port2func(u_int16_t);

#endif

/* ip_services.h */
