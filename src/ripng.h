/**************************************************************************** 
** File: ripng.h
**
** Author: Mike Borella
**
** Comments: Structure of RIPng packets
**
** $Id: ripng.h,v 1.6 2007/06/25 13:06:29 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef RIPNG_H
#define RIPNG_H

#include "global.h"
#include "local.h"

/*
 * RIP commands
 */

#define RIPNG_CMD_RQ         1
#define RIPNG_CMD_RP         2

/*
 * Static part of RIPng header
 */

typedef struct ripng_header
{
  u_int8_t   command;
  u_int8_t   version;
  u_int16_t  mbz;
} ripng_header_t;

/*
 * RIPng route header
 */

typedef struct ripng_route_header
{
  u_int8_t   address[16];
  u_int16_t  route_tag;
  u_int8_t   netmask;
  u_int8_t   metric;
} ripng_route_header_t;

void dump_ripng(packet_t *pkt);

#endif
