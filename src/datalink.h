/**************************************************************************** 
** File: datalink.h
**
** Author: Mike Borella
**
** Header file for generic datalink functionality
**
** $Id: datalink.h,v 1.3 2001/11/02 00:23:06 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef DATALINK_H
#define DATALINK_H

#include "global.h"

#define DATALINK_TYPE_ETHERNET   0
#define DATALINK_TYPE_8023       1
#define DATALINK_TYPE_TOKENBUS   2
#define DATALINK_TYPE_TOKENRING  3
#define DATALINK_TYPE_METRONET   4
#define DATALINK_TYPE_HDLC       5
#define DATALINK_TYPE_CHARSYNCH  6
#define DATALINK_TYPE_IBMC2C     7
#define DATALINK_TYPE_FDDI       8
#define DATALINK_TYPE_NULL       9
#define DATALINK_TYPE_SLIP       10
#define DATALINK_TYPE_PPP        11
#define DATALINK_TYPE_RAWIP      12

/*
 * Function prototypes
 */

void datalink_pcap(u_char *, const struct pcap_pkthdr *, u_char *);
void datalink(int, struct timeval, u_int32_t, u_int32_t, u_int8_t *);

#endif
