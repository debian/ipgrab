/**************************************************************************** 
**
** File: raw.c
**
** Author: Mike Borella
**
** Comments: Dump raw data link (IP) packets
**
** $Id: raw.c,v 1.5 2001/05/28 19:24:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "raw.h"
#include "ip_protocols.h"

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_raw()
**
** Process packets from the DLT_RAW interface type
**
**----------------------------------------------------------------------------
*/

void dump_raw(packet_t *pkt)
{

  /* Set the layer */
  set_layer(LAYER_DATALINK);

  /*
   * Dump header announcement
   */

  if (my_args->m)
    {
      /* raw header announcement need not be displayed */
    }
  else
    {
      display_header_banner_ts("Raw IP", pkt->timestamp);
    }

  dump_ip(pkt);
}
