/**************************************************************************** 
** File: pptp.h
**
** Author: Mike Borella
**
** Comments: Generic PPTP header structure
**
** $Id: pptp.h,v 1.5 2000/09/27 23:16:51 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef PPTP_H
#define PPTP_H

#include "global.h"
#include "local.h"

#define PPTP_HOSTNAME_LEN    64
#define PPTP_VENDOR_LEN      64
#define PPTP_PHONENUM_LEN    64
#define PPTP_SUBADDRESS_LEN  64

/*
 * PPTP message types 
 */

#define PPTP_MSGTYPE_CONTROL    1
#define PPTP_MSGTYPE_MANAGEMENT 2

/*
 * PPTP control message types
 */

#define PPTP_CNTLMSGTYPE_STARTCCREQ   1
#define PPTP_CNTLMSGTYPE_STARTCCREP   2
#define PPTP_CNTLMSGTYPE_STOPCCREQ    3 
#define PPTP_CNTLMSGTYPE_STOPCCREP    4
#define PPTP_CNTLMSGTYPE_ECHOREQ      5
#define PPTP_CNTLMSGTYPE_ECHOREP      6
#define PPTP_CNTLMSGTYPE_OUTGOINGREQ  7
#define PPTP_CNTLMSGTYPE_OUTGOINGREP  8
#define PPTP_CNTLMSGTYPE_INCOMINGREQ  9
#define PPTP_CNTLMSGTYPE_INCOMINGREP  10
#define PPTP_CNTLMSGTYPE_INCOMINGCONN 11
#define PPTP_CNTLMSGTYPE_CALLCLEARREQ 12
#define PPTP_CNTLMSGTYPE_CALLDISCNTFY 13
#define PPTP_CNTLMSGTYPE_WANERRORNTFY 14
#define PPTP_CNTLMSGTYPE_SETLINKINFO  15

/* 
 * Structure of generic PPTP header - should be in all PPTP packets
 *
 *
 *     0                   1                   2                   3
 *     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *    |             Length            |       PPTP Message Type       |
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *    |                         Magic Cookie                          |
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *    |     Control Message Type      |           Reserved0           |
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

typedef struct pptp_generic_header
{
  u_int16_t length;
  u_int16_t pptp_msg_type;
  u_int32_t cookie;
  u_int16_t control_msg_type;
  u_int16_t reserved0;
  
} pptp_generic_header_t;

/*
 * Structure of PPTP Start-Control-Connection-Request
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |       Protocol Version        |           Reserved1           |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                     Framing Capabilities                      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                      Bearer Capabilities                      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |       Maximum Channels        |       Firmware Revision       |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * +                     Host Name (64 octets)                     +
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * +                   Vendor String (64 octets)                   +
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

typedef struct pptp_startccreq
{
  u_int16_t version;
  u_int16_t reserved1;
  u_int32_t framing_cap;
  u_int32_t bearer_cap;
  u_int16_t max_channels;
  u_int16_t firmware_rev;
  u_int8_t  hostname[PPTP_HOSTNAME_LEN];
  u_int8_t  vendor[PPTP_VENDOR_LEN];
} pptp_startccreq_t;


/*
 * Structure of PPTP Start-Control-Connection-Reply
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |       Protocol Version        |  Result Code  |  Error Code   |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                     Framing Capabilities                      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                      Bearer Capabilities                      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |       Maximum Channels        |       Firmware Revision       |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * +                     Host Name (64 octets)                     +
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * +                   Vendor String (64 octets)                   +
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

typedef struct pptp_startccrep
{
  u_int16_t version;
  u_int8_t  result_code;
  u_int8_t  error_code;
  u_int32_t framing_cap;
  u_int32_t bearer_cap;
  u_int16_t max_channels;
  u_int16_t firmware_rev;
  u_int8_t  hostname[PPTP_HOSTNAME_LEN];
  u_int8_t  vendor[PPTP_VENDOR_LEN];
} pptp_startccrep_t;


/*
 * Structure of PPTP Outgoing-Call-Request
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |            Call ID            |      Call Serial Number       |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                          Minimum BPS                          |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                          Maximum BPS                          |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                          Bearer Type                          |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                         Framing Type                          |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |   Packet Recv. Window Size    |    Packet Processing Delay    |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |      Phone Number Length      |           Reserved1           |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * +                   Phone Number (64 octets)                    +
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * +                    Subaddress (64 octets)                     +
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

typedef struct pptp_outgoingreq
{
  u_int16_t call_id;
  u_int16_t call_sn;
  u_int32_t min_bps;
  u_int32_t max_bps;
  u_int32_t bearer_type;
  u_int32_t framing_type;
  u_int16_t recv_window_size;
  u_int16_t packet_proc_delay;
  u_int16_t phone_num_len;
  u_int16_t reserved1;
  u_int8_t  phone_num[PPTP_PHONENUM_LEN];
  u_int8_t  subaddress[PPTP_SUBADDRESS_LEN];
} pptp_outgoingreq_t;


/*
 * Structure of PPTP Outgoing-Call-Reply
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |            Call ID            |       Peer's Call ID          |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |  Result Code  |  Error Code   |          Cause Code           |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                         Connect Speed                         |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |   Packet Recv. Window Size    |    Packet Processing Delay    |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                      Physical Channel ID                      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

typedef struct pptp_outgoingrep
{
  u_int16_t call_id;
  u_int16_t peer_call_id;
  u_int8_t  result_code;
  u_int8_t  error_code;
  u_int16_t cause_code;
  u_int32_t connect_speed;
  u_int16_t recv_window_size;
  u_int16_t packet_proc_delay;
  u_int32_t phy_channel_id;
} pptp_outgoingrep_t;


/*
 * Structure of PPTP Set-Link-Info
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |        Peer's Call ID         |           Reserved1           |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                           Send ACCM                           |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                         Receive ACCM                          |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

typedef struct pptp_setlinkinfo
{
  u_int16_t  peer_call_id;
  u_int16_t  reserved1;
  u_int32_t  send_accm;
  u_int32_t  receive_accm;
} pptp_setlinkinfo_t;


/*
 * Structure of PPTP Call-Clear-Request
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |            Call ID            |           Reserved1           |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 */

typedef struct pptp_callclearreq
{
  u_int16_t  call_id;
  u_int16_t  reserved;
} pptp_callclearreq_t;


/*
 * Structure of PPTP Call-Disconnect-Notify
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |            Call ID            |  Result Code  |  Error Code   |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |          Cause Code           |           Reserved1           |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * +              Call Statistics (128 octets)                     +
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 */

typedef struct pptp_calldiscntfy
{
  u_int16_t  call_id;
  u_int8_t   result_code;
  u_int8_t   error_code;
  u_int16_t  cause_code;
  u_int16_t  reserved;
  u_int8_t   call_stats[128];
} pptp_calldiscntfy_t;

/*
 * Main PPTP function
 */

void dump_pptp(packet_t *);

#endif
