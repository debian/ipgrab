/**************************************************************************** 
** File: ah.h
**
** Author: Mike Borella
**
** Comments: Structure of AH packets
**
** $Id: ah.h,v 1.5 2007/06/25 12:38:54 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef AH_H
#define AH_H

#include "global.h"
#include "local.h"

/*
 * Static part of AH header
 */

typedef struct ah_header
{
#if defined(WORDS_BIGENDIAN)
  u_int32_t next:8,
	    length:8,
	    reserved:16;
#else
  u_int32_t reserved:16,
            length:8,
	    next:8;
#endif
  u_int32_t spi;
  u_int32_t seqno;
  
} ah_header_t;

void dump_ah(packet_t *);

#endif
