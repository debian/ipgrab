/**************************************************************************** 
**
** File: open_pcap.c
**
** Author: Mike Borella
**
** Set up pcap to sniff the packets we want.  Most of these commands are 
** listed in the pcap(3) man page.
**
** $Id: open_pcap.c,v 1.4 2001/03/26 17:00:32 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "open_pcap.h"
#include "error.h"
#include "parse_cl.h"

#define SNAPLEN      1514
#define PROMISC      1
#define READ_TIMEOUT 500

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
 *
 * pcap_open()
 *
 *----------------------------------------------------------------------------
 */

int open_pcap(void)
{
  extern char        * pcap_cmd;
  extern pcap_t      * pd;
  bpf_u_int32          localnet;
  bpf_u_int32          netmask;
  struct bpf_program   fcode;
  char                 errorbuf[PCAP_ERRBUF_SIZE];
  int                  datalink;

  /*
   * If -r and a filename are given, then take input from the file rather 
   * than from the network connection.  Otherwise, Look up the device and 
   * get a handle to it
   */

  if (my_args->r != NULL)
    {
      fprintf(stderr, "Reading from file %s\n", my_args->r);
      pd = pcap_open_offline(my_args->r, errorbuf);
      if (pd == NULL)
	error_fatal("%s", errorbuf);
      localnet = 0;
      netmask = 0;
    }
  else
    {
      if (my_args->i == NULL) 
	{
	  my_args->i = pcap_lookupdev(errorbuf);
	  if (my_args->i == NULL) 
	    error_fatal("open_pcap: pcap_lookupdev() failed for %s: %s", 
	  		my_args->i, errorbuf);
	}
      
      /*
       * Get a file descriptor to the device
       */
      
      pd = pcap_open_live(my_args->i, SNAPLEN, PROMISC, READ_TIMEOUT, 
			  errorbuf);
      if (pd == NULL) 
	error_fatal("open_pcap: pcap_open_live() failed for %s: %s", 
  		    my_args->i, errorbuf);
      
      /*
       * Determine local net and netmask
       */
      
      if (pcap_lookupnet(my_args->i, &localnet, &netmask, errorbuf) < 0)
	error_fatal("open_pcap: pcap_lookupnet() failed for %s: %s", 
		    my_args->i, errorbuf);
    }
  
  /*
   * Compile command line filter spec info fcode FSM
   */

  if (pcap_compile(pd, &fcode, pcap_cmd, 0, netmask) < 0)
    error_fatal("pcap_compile: %s", pcap_geterr(pd));

  /*
   * Set the pcap filter with our fcode FSM.  That should do it...
   */

  if (pcap_setfilter(pd, &fcode) < 0)
    error_fatal("pcap_setfilter: %s", pcap_geterr(pd));

  /*
   * Get the data link type 
   */

  datalink = pcap_datalink(pd);
  if (datalink < 0) 
    error_fatal("pcap_datalink: %s", pcap_geterr(pd));

  return datalink;
}
