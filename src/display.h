/**************************************************************************** 
** File: display.h
**
** Author: Mike Borella
**
** Comments: Header file for displaying functions
** 
** $Id: display.h,v 1.23 2006/11/21 07:47:33 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef DISPLAY_H
#define DISPLAY_H

#include "global.h"
#include "strmap.h"
#include "utilities.h"
#include "layers.h"
#include "error.h"


/*
 * Display format types
 */

typedef enum display
{
  DISP_DEC,             /* Display and arbitrary decimal value */
  DISP_BIN,             /* Binary format with leading 0's (01101110) */
  DISP_BINNLZ,          /* Binary format w/o leading 0's (1101110) */
  DISP_HEX,             /* 2 bytes, hex (0F) */
  DISP_HEX_MULTILINE,   /* Hex output spanning more than one line */
  DISP_HEXCOLONS,       /* Hex bytes separated by colons (0F:7A:B1) */
  DISP_HEXCOLONS4,      /* 4 Hex bytes between colons */
  DISP_DOTTEDDEC,       /* Dotted decimal (IP address) format (4 bytes only) */
  DISP_STRING,          /* Plain old ascii string */
  DISP_STRING_MULTILINE /* String output spanning more than one line */
} display_t;

/*
 * Header line types
 */

typedef enum headerline
{
  HEADERLINE_DASHES,
  HEADERLINE_STARS,
  HEADERLINE_EQUALS,
  HEADERLINE_DOTS,  
} headerline_t;

/*
 * Function prototypes
 */

inline void display_header_line (headerline_t);
inline void display_header_banner (char *);
inline void display_header_banner_ts(char *, char *);
inline void display (char *, u_int8_t *, u_int32_t, display_t);
inline void display_string (u_int8_t *, u_int8_t *);
inline void display_strmap(char *, int, strmap_t *map);
inline void display_strmap_hex(char *, int, strmap_t *map);
inline void display_ipv4 (u_int8_t *, u_int8_t *);
inline void display_ipv6 (u_int8_t *, u_int8_t *);
inline void display_minimal (u_int8_t *, u_int32_t, display_t);
inline void display_minimal_string (u_int8_t *);
inline void display_minimal_ipv4 (u_int8_t *);
inline void display_minimal_ipv6 (u_int8_t *);
inline void display_minimal_cr(void);

#endif
