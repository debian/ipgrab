/**************************************************************************** 
** File: ns_labels.h
**
** Author: Mike Borella
**
** Comments: Encapsulation for ugly DNS / NETBIOS NS label manipulation
**
** $Id: ns_labels.h,v 1.6 2006/11/21 07:47:34 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef NS_LABELS_H
#define NS_LABELS_H

#include "global.h"
#include "local.h"

/*
 * This is for parsing DNS and NETBIOS NS labels.  We save all labels so 
 * that we can refer to them without re-reading the packet.  
 * This is SO annoying!  Goes to show the damage that can be done when 
 * bandwidth is a major concern.
 */

#define MAX_LABEL_SIZE 64

typedef struct ns_label
{
  int32_t offset;                   /* offset from beginning of DNS payload */
  char label[MAX_LABEL_SIZE];       /* char string contents of label */
  int next;                         /* next label */
} ns_label_t;

void reset_nslabels(void);
void new_nslabel(void);
void set_nslabel_offset(int32_t);
void set_nslabel_label(char *);
void set_nslabel_next(void);
void get_nslabel(int32_t, char *);   /* get a label given its offset */
void parse_ns_labels(packet_t *, char *);

#endif
