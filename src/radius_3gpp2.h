/**************************************************************************** 
** File: radius_3gpp2.h
**
** Author: Mike Borella
**
** Comments: Header file for RADIUS 3GPP2 vendor specific attribute support.
**
** $Id: radius_3gpp2.h,v 1.1 2001/11/21 22:03:56 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef RADIUS_3GPP2_H
#define RADIUS_3GPP2_H

#include "global.h"
#include "local.h"

void dump_radius_3gpp2(packet_t *, u_int8_t, u_int8_t);

#endif
