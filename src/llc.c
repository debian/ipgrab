/**************************************************************************** 
** File: llc.c
**
** Author: Mike Borella
**
** Comments: Dump LLC packets
**
** $Id: llc.c,v 1.6 2007/06/25 11:36:23 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "llc.h"
#include "ipx.h"

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_llc()
**
** Process packets encapsulated in LLC
**
**----------------------------------------------------------------------------
*/

void dump_llc(packet_t *pkt)
{
  llc_header_t llc;
  u_int8_t     org_id[3];
  u_int16_t    ethertype;
  u_int8_t     control;

  /* Set the layer */
  set_layer(LAYER_DATALINK);

  /*
   * Read the header
   */

  if (get_packet_bytes((u_int8_t *) &llc, pkt, 2) == 0)
    return;

  /*
   * Dump header
   */
  if (my_args->m)
    {
      display_minimal_string("| LLC ");
      display_minimal((u_int8_t *) &llc.dsap, 1, DISP_HEX);
      display_minimal_string(" ");
      display_minimal((u_int8_t *) &llc.ssap, 1, DISP_HEX);
      display_minimal_string(" ");
    }
  else
    {
      /* announcement */
      display_header_banner("LLC header");
      display("DSAP", (u_int8_t *) &llc.dsap, 1, DISP_HEX);
      display("SSAP", (u_int8_t *) &llc.ssap, 1, DISP_HEX);
    }

  /* check for IPX raw framing */
  if (llc.dsap == 0xFF && llc.ssap == 0xFF)
    {
      dump_ipx(pkt);
      return;
    }

  /*
   * If it was not IPX raw framing, now we know that we have
     a control field as well in the packet. So get it.
   */

  if (get_packet_bytes((u_int8_t *) &control, pkt, 1) == 0)
    return;

  if (my_args->m)
    {
      display_minimal((u_int8_t *) &control, 1, DISP_HEX);
      display_minimal_string(" ");
    }
  else
    display("Control", (u_int8_t *) &control, 1, DISP_HEX);


  /* check for NETBIOS logon */
  if (llc.dsap == 0xF0 && llc.ssap == 0xF0)
    {
      /* dump_ipx(pkt); */
      return;
    }

  /* check for IPX/NETBIOS encapsulation */
  if (llc.dsap == 0xE0 && llc.ssap == 0xE0)
    {
      dump_ipx(pkt);
      return;
    }

  /* check for SNAP (802.1a) encapsulation */
  if (llc.dsap == 0xAA && llc.ssap == 0xAA)
    {
      /* The next three bytes should be the organization id, which is 
	 typically 0 */
      
      if (get_packet_bytes((u_int8_t *) org_id, pkt, 3) == 0)
	return;

      if (get_packet_bytes((u_int8_t *) &ethertype, pkt, 2) == 0)
	return;
      
      /*
       * Conversions -- No conversion is required as the DISP_HEX
       * option in display:display_minimal() requires data in
       * network byte order i.e. big endian unless DISP_HEX is
       * modified. Later option would require looking up the entire
       * source code so as not to break it.
       */

      /*
       * ethertype = ntohs(ethertype);
       */

      /*
       * Display fields 
       */

      if (my_args->m)
	{
	  display_minimal_string("SNAP ");
	  display_minimal((u_int8_t *) &ethertype, 2, DISP_HEX);
	  display_minimal_string(" ");
	}
      else
	{
	  /* announcement */
	  display_header_banner("SNAP header");
	  display("Organization ID", (u_int8_t *) org_id, 3, DISP_HEX);
	  display("Protocol", (u_int8_t *) &ethertype, 2, DISP_HEX);
	}
      
    }
      
  

  return;
}

