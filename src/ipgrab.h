/**************************************************************************** 
**
** File: ipgrab.h
**
** Author: Mike Borella
**
** Comments: Macros and stuff
**
** $Id: ipgrab.h,v 1.2 2000/08/29 21:59:02 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"

char *copy_argv(char **argv);

/*
 * This is a typecast to avoid stupid warning messages.  The PCAP library
 * assumes that a call to a function that parses data link layer packets
 * includes a const in the third argument.  Actually doing so is a pain
 * because it makes the higher layer protocols much harder to parse because
 * you can't assign a pointer to the packet data.  So this tricks PCAP
 * into thinking you're using the const when you really are not.
 * Bleah.
 */

typedef void (*pcap_func_t)(u_char *, const struct pcap_pkthdr *, 
			    const u_char *);
