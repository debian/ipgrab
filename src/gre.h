/**************************************************************************** 
** File: gre.h
**
** Author: Mike Borella
**
** Comments: Generic routing encapsulation header file
**
** $Id: gre.h,v 1.3 2001/03/28 00:43:18 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef GRE_H
#define GRE_H

#include "global.h"
#include "local.h"

/*
 * GRE version 0 header (RFC 1701)
 */

typedef struct gre_v0_rfc1701
{
#ifdef WORDS_BIGENDIAN
  u_int8_t  c:1,
            r:1,
            k:1,
            S:1,
            s:1,
	    recur:3;
  u_int8_t  flags:5,
            version:3;
#else
  u_int8_t  recur:3,
            s:1,
            S:1,
            k:1,
            r:1,
            c:1;
  u_int8_t  version:3,
            flags:5;
#endif
  u_int16_t protocol;
} gre_v0_rfc1701_t;

/*
 * GRE version 0 header (RFC 2784)
 */

typedef struct gre_v0_rfc2784
{
#ifdef WORDS_BIGENDIAN
  u_int8_t  c:1,
            reserved_high:7;
  u_int8_t  reserved_low:5,
            version:3;
#else
  u_int8_t  reserved_high:7,
            c:1;
  u_int8_t  version:3,
            reserved_low:5;
#endif
  u_int16_t protocol;
} gre_v0_rfc2784_t;

/*
 * GRE version 1 header (used for PPTP, etc.)
 */

typedef struct gre_v1
{
#ifdef WORDS_BIGENDIAN
  u_int8_t  C_bit:1,
            R_bit:1,
            K_bit:1,
            S_bit:1,
            s_bit:1,
            recur:3;
  u_int8_t  A_bit:1,
            flags:4,
            version:3;
#else
  u_int8_t  recur:3,
            s_bit:1,
            S_bit:1,
            K_bit:1,
            R_bit:1,
            C_bit:1;
  u_int8_t  version:3,
            flags:4,
            A_bit:1;
#endif
  u_int16_t protocol;
  u_int16_t payload_len;
  u_int16_t call_id;
} gre_v1_t;

/*
 * GRE function
 */

void dump_gre(packet_t *);

#endif
