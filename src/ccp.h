/**************************************************************************** 
** File: ccp.h
**
** Author: Mike Borella
**
** Comments: CCP module definitions.
**
** $Id: ccp.h,v 1.1 2001/10/02 21:36:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef CCP_H
#define CCP_H

#include "global.h"
#include "local.h"

void dump_ccp(packet_t *);

#endif
