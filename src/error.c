/**************************************************************************** 
**
** File: error.c
**
** Author: Mike Borella
**
** Try to gracefully handle errors.  
**
** $Id: error.c,v 1.4 2007/06/25 10:01:12 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include <errno.h>
#include "error.h"

#define ERROR_BUF_SIZE 32


/*
 * This avoids a warning with glibc compilation 
 */

#ifndef errno
extern int errno;
#endif

/*
 * If we have strerror() we won't use sys_errlist, so don't bother defining
 * it.
 */

#ifndef HAVE_STRERROR
extern char * sys_errlist[];
#endif

/*----------------------------------------------------------------------------
**
** Function: my_strerror()
**
** Comments: Use strerror() if this system supports it. Roll our own otherwise.
**
**----------------------------------------------------------------------------
*/

char *my_strerror(int errnum)
{
#ifdef HAVE_STRERROR
  return (strerror(errnum));
#else
  extern int sys_nerr;
  static char ebuf[ERROR_BUF_SIZE];
  
  if (errnum < sys_nerr)
    return ((char *) sys_errlist[errnum]);
  snprintf(ebuf, ERROR_BUF_SIZE, "Unknown error: %d", errnum);
  return(ebuf);
#endif
}

/*----------------------------------------------------------------------------
**
** error()
**
** Generic error dumping.  Here's how the arguments are used
**
** syserr: 0 is not an error from a system call, non-zero otherwise
** filename: filename of the caller.  Optional.
** line: line number of the caller.  Optional, set to 0 is filename and 
**       line number info is not to be printed
**
**----------------------------------------------------------------------------
*/

void error(int syserr, char *filename, int line, const char *fmt, va_list args)
{
  /*
   * Dump the error message
   */

  fprintf(stderr,"ERROR: ");
  if (line)
    fprintf(stderr, "%s line %d: ", filename, line);
  vfprintf(stderr, fmt, args);

  /*
   * If this is a system error, print the errno info.  Otherwise, don't
   */

  if (syserr)
    {
      fprintf(stderr," (%d ", errno);
      fprintf(stderr, "%s)\n", my_strerror(errno));
    }
  else
    fprintf(stderr, "\n");
}


/*****************************************************************************
 * Externally-visible functions are below here
 ****************************************************************************/

/*----------------------------------------------------------------------------
**
** error_fatal()
**
** General fatal error.
**
**----------------------------------------------------------------------------
*/

void error_fatal(char *fmt, ...)
{
  va_list       args;

  va_start(args,fmt);
  error(0, "", 0, fmt, args);
  va_end(args);
  abort();
}

/*----------------------------------------------------------------------------
**
** error_system()
**
** General system error.
**
**----------------------------------------------------------------------------
*/

void error_system(char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);
  error(1, "", 0, fmt, args);
  va_end(args);
  abort();
}

/*----------------------------------------------------------------------------
**
** error_message()
**
** General non-terminal error.
**
**----------------------------------------------------------------------------
*/

void error_message(char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);
  error(1, "", 0, fmt, args);
  va_end(args);
}
