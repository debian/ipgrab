/**************************************************************************** 
** File: icmp.h
**
** Author: Mike Borella
**
** Comments: Generic ICMP header format for fixed portion
**
** $Id: icmp.h,v 1.6 2001/01/20 00:30:31 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef ICMP_H
#define ICMP_H

#include "global.h"
#include "local.h"

typedef struct icmp_header
{
  u_int8_t type;
  u_int8_t code;
  u_int16_t checksum;
} icmp_header_t;


/*
 * Let's list all of the ICMP types here.  Whoo-hoo!
 * Actually, these are only the ones made "official" by an RFC.
 */

#define ICMP_TYPE_ECHOREPLY       0
#define ICMP_TYPE_DESTUNREACHABLE 3
#define ICMP_TYPE_SOURCEQUENCH    4
#define ICMP_TYPE_REDIRECT        5
#define ICMP_TYPE_ECHOREQUEST     8
#define ICMP_TYPE_ROUTERADVERT    9
#define ICMP_TYPE_ROUTERSOLICIT   10
#define ICMP_TYPE_TIMEEXCEEDED    11
#define ICMP_TYPE_PARAMPROBLEM    12
#define ICMP_TYPE_TIMESTAMP       13
#define ICMP_TYPE_TIMESTAMPREPLY  14
#define ICMP_TYPE_INFOREQUEST     15
#define ICMP_TYPE_INFOREPLY       16
#define ICMP_TYPE_MASKREQUEST     17
#define ICMP_TYPE_MASKREPLY       18
#define ICMP_TYPE_TRACEROUTE      30
#define ICMP_TYPE_CONVERSIONERROR 31

/*
 * These are the ICMP destination unreachable codes
 */

#define ICMP_DU_NET          0
#define ICMP_DU_HOST         1
#define ICMP_DU_PROTOCOL     2
#define ICMP_DU_PORT         3
#define ICMP_DU_FRAG         4
#define ICMP_DU_SRCRTEFAIL   5
#define ICMP_DU_NETUNKNOWN   6
#define ICMP_DU_HOSTUNKNOWN  7
#define ICMP_DU_SRCISOLATED  8
#define ICMP_DU_NETADMIN     9
#define ICMP_DU_HOSTADMIN    10
#define ICMP_DU_NETTOS       11
#define ICMP_DU_HOSTTOS      12
#define ICMP_DU_ADMIN        13
#define ICMP_DU_HOSTPRECVIOL 14
#define ICMP_DU_PRECCUTOFF   15

/*
 * These are the ICMP time exceeded codes
 */

#define ICMP_TIMEEXCEEDED_TTL        0
#define ICMP_TIMEEXCEEDED_REASSEMBLY 1

/*
 * Function prototypes
 */

void dump_icmp(packet_t *);

#endif
