/**************************************************************************** 
** File: ipx.h
**
** Author: Mike Borella
**
** Comments: IPX header format and such
**
** $Id: ipx.h,v 1.6 2006/11/21 07:47:34 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef IPX_H
#define IPX_H

#include "global.h"
#include "local.h"

/*
 * IPX packet types
 */

#define IPX_PACKETTYPE_UNKNOWN     0
#define IPX_PACKETTYPE_RIP         1
#define IPX_PACKETTYPE_ECHO        2
#define IPX_PACKETTYPE_ERROR       3
#define IPX_PACKETTYPE_PEP         4
#define IPX_PACKETTYPE_SPX         5
#define IPX_PACKETTYPE_NCP         17
#define IPX_PACKETTYPE_NETBIOS     20

/*
 * IPX header 
 */

typedef struct ipx_header
{
  u_int16_t csum;
  u_int16_t len;
  u_int8_t  tc;
  u_int8_t  pt;
  u_int32_t dstnet;
  u_char    dstnode[6];
  u_int16_t dstport;
  u_int32_t srcnet;
  u_char    srcnode[6];
  u_int16_t srcport;
} ipx_header_t;

void dump_ipx(packet_t *);

#endif



