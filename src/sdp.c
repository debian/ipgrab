/**************************************************************************** 
** File: sdp.c
**
** Author: Mike Borella
**
** Comments: Dump SDP header information. Like SIP, we just dump the 
** Header contents in plaintext.
**
** Maybe we'll try to "decode" these headers into something intelligble 
** later...
**
** $Id: sdp.c,v 1.8 2001/05/28 19:24:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "sdp.h"

#define LINE_SIZE 1500

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_sdp()
**
** Parse SDP and dump fields
**
**----------------------------------------------------------------------------
*/

void dump_sdp(packet_t *pkt, int length)
{
  char line[LINE_SIZE];
  int  len;
  int  total_length = 0;

  /* Set the layer */
  set_layer(LAYER_APPLICATION);

  if (my_args->m)
    {
      display_minimal_string("SDP ");
      /* maybe do something more intelligent here... */
    }
  else
    {
      /* announcement */
      display_header_banner("SDP Headers");
      
      /* get each line one at a time */
      while(1)
	{
	  len = get_packet_line(line, length-total_length, pkt);
	  if (len <= 0)
	    break;
	  total_length += len;
	  display("Header", (u_int8_t *) line, len, DISP_STRING_MULTILINE);
	  if (total_length > length)
	    break;
	}  

      /* dump the hexbuffer */
      hexbuffer_flush();

    }
}

