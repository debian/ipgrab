/**************************************************************************** 
**
** File: stats.h
**
** Author: Mike Borella
**
** Header file for statistics collection
**
** $Id: stats.h,v 1.2 2001/09/07 19:27:02 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef STATS_H
#define STATS_H

#include "global.h"

/* 
 * Types of packets to collect stats on
 */

#define STATS_IP         0
#define STATS_UDP        1
#define STATS_TCP        2
#define STATS_IPX        3
#define STATS_GRE        4
#define STATS_ICMP       5
#define STATS_IPV6       6
#define STATS_ICMPV6     7
#define STATS_OSPF       8
#define STATS_RSVP       9
#define STATS_ARP        10
#define STATS_AH         11
#define STATS_ESP        12
#define STATS_IGMP       13

/*
 * Function prototypes
 */

inline void stats_init(void);
inline void stats_update(int type);
inline void stats_dump(void);
inline void stats_reset(void);
inline void stats_pause(void);
inline void stats_unpause(void);

#endif
