/**************************************************************************** 
** File: local.h
**
** Author: Mike Borella
**
** Comments: Includes the ipgrab header files commonly needed.
**
** $Id: local.h,v 1.6 2001/11/15 20:15:59 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef LOCAL_H
#define LOCAL_H

#include "strmap.h"
#include "display.h"
#include "packet_manip.h"
#include "parse_cl.h"
#include "hexbuffer.h"
#include "utilities.h"
#include "stats.h"
#include "layers.h"
#include "state.h"
#include "ip_services.h"

/*
 * This is a stupid hack to get around forcing signal handlers to take
 * an int as an argument
 */

typedef void (*sighandler_t)(int);

#endif
