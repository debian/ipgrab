/**************************************************************************** 
** File: ip.h
**
** Author: Mike Borella
**
** Comments: Generic IP header structure - an attempt at OS independence
**
** $Id: ip.h,v 1.7 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef IP_H
#define IP_H

#include "global.h"
#include "local.h"

typedef struct ip_header
{
#if defined(WORDS_BIGENDIAN)
  u_int8_t       version:4, 
                 header_length:4;
#else
  u_int8_t       header_length:4, 
                 version:4;
#endif
  u_int8_t       tos;
  u_int16_t      length;
  u_int16_t      id;
  u_int16_t      offset;
  u_int8_t       ttl;
  u_int8_t       protocol;
  u_int16_t      checksum;
  u_int32_t      src;
  u_int32_t      dst;
} ip_header_t;

typedef struct ip_option
{
  u_int8_t code;
  u_int8_t length;
} ip_option_t;

#define IP_OPTION_EOL          0
#define IP_OPTION_NOP          1
#define IP_OPTION_RECORDROUTE  7
#define IP_OPTION_TIMESTAMP    68
#define IP_OPTION_TRACEROUTE   82
#define IP_OPTION_SECURITY     130
#define IP_OPTION_LSR          131
#define IP_OPTION_EXTSECURITY  133
#define IP_OPTION_COMSECURITY  134
#define IP_OPTION_STREAMID     136
#define IP_OPTION_SSR          137
#define IP_OPTION_ROUTERALERT  148

void dump_ip(packet_t *);

#endif
