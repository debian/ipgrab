/**************************************************************************** 
** File: iana.c
**
** Author: Mike Borella
**
** Comments: IANA specific definitions
**
** $Id: iana.c,v 1.1 2001/11/21 18:04:12 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "iana.h"

/*
 * IANA enterprise numbers
 */

strmap_t iana_enterprise_map[] = 
{
  { IANA_ENTERPRISE_CISCO,              "Cisco" },
  { IANA_ENTERPRISE_3COM,               "3Com" },
  { IANA_ENTERPRISE_3COMCARRIER,        "3Com Carrier Systems" },
  { IANA_ENTERPRISE_3GPP2,              "3GPP2" },
  { IANA_ENTERPRISE_COMMWORKS,          "CommWorks" },
  { 0, "" }
};
