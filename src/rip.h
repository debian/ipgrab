/**************************************************************************** 
** File: rip.h
**
** Author: Mike Borella
**
** Comments: Structure of RIP packets
**
** $Id: rip.h,v 1.6 2007/06/25 13:04:46 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef RIP_H
#define RIP_H

#include "global.h"
#include "local.h"

/*
 * RIP commands
 */

#define RIP_CMD_RQ         1
#define RIP_CMD_RP         2
#define RIP_CMD_POLL       5
#define RIP_CMD_POLL_ENTRY 6

/*
 * Static part of RIP v1 header
 */

typedef struct ripv1_header
{
  u_int8_t   command;
  u_int8_t   version;
  u_int16_t  mbz;
} ripv1_header_t;

/*
 * RIP v1 route header
 */

typedef struct ripv1_route_header
{
  u_int16_t   addr_fam;
  u_int16_t   mbz1;
  u_int32_t   ipaddr;
  u_int8_t    mbz2[8];
  u_int32_t   metric;
} ripv1_route_header_t;

/*
 * Static part of RIP v2 header
 */

typedef struct ripv2_header
{
  u_int8_t   command;
  u_int8_t   version;
  u_int16_t  domain;
} ripv2_header_t;

/*
 * RIP v2 route header
 */

typedef struct ripv2_route_header
{
  u_int16_t   addr_fam;
  u_int16_t   route_tag;
  u_int32_t   ipaddr;
  u_int32_t   netmask;
  u_int32_t   next_hop;
  u_int32_t   metric;
} ripv2_route_header_t;

/*
 * RIP v2 authentication header
 */

typedef struct ripv2_auth_header
{
  u_int16_t  addr_fam;
  u_int16_t  type;
  u_int8_t   passwd[16];
} ripv2_auth_header_t;

void dump_rip(packet_t *pkt);

#endif
