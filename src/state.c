/**************************************************************************** 
**
** File: state.c
**
** Author: Mike Borella
**
** Comments: Support for per-packet state.  Basically, this module allows
** us to save a handful of information about each packet, such as IP 
** addresses and port numbers.  Then this information is available to 
** application layer decodes.  One example where this is necessary is 
** TFTP.
**
** $Id: state.c,v 1.2 2007/06/25 11:42:57 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "state.h"

/* store the packet state in this data structure */
typedef struct packet_state
{
  u_int32_t srcaddr;
  u_int32_t dstaddr;
  u_int16_t srcport;
  u_int16_t dstport;  
} packet_state_t;

static packet_state_t state;

/*----------------------------------------------------------------------------
**
** state_set_srcaddr(u_int32_t)
**
** Set source IP address
** 
**----------------------------------------------------------------------------
*/

void state_set_srcaddr(u_int32_t a)
{
  state.srcaddr = a;
}

/*----------------------------------------------------------------------------
**
** state_set_dstaddr(u_int32_t)
**
** Set destination IP address
** 
**----------------------------------------------------------------------------
*/

void state_set_dstaddr(u_int32_t a)
{
  state.dstaddr = a;
}

/*----------------------------------------------------------------------------
**
** state_set_srcport(u_int16_t)
**
** Set source port
** 
**----------------------------------------------------------------------------
*/

void state_set_srcport(u_int16_t p)
{
  state.srcport = p;
}

/*----------------------------------------------------------------------------
**
** state_set_dstport(u_int16_t)
**
** Set destination port
** 
**----------------------------------------------------------------------------
*/

void state_set_dstport(u_int16_t p)
{
  state.dstport = p;
}

/*----------------------------------------------------------------------------
**
** state_get_srcaddr()
**
** Get source IP address
** 
**----------------------------------------------------------------------------
*/

u_int32_t state_get_srcaddr(void)
{
  return state.srcaddr;
}

/*----------------------------------------------------------------------------
**
** state_get_dstaddr()
**
** Get destination IP address
** 
**----------------------------------------------------------------------------
*/

u_int32_t state_get_dstaddr(void)
{
  return state.dstaddr;
}

/*----------------------------------------------------------------------------
**
** state_get_srcport()
**
** Get source port
** 
**----------------------------------------------------------------------------
*/

u_int16_t state_get_srcport(void)
{
  return state.srcport;
}

/*----------------------------------------------------------------------------
**
** state_get_dstport()
**
** Get destination port
** 
**----------------------------------------------------------------------------
*/

u_int16_t state_get_dstport(void)
{
  return state.dstport;
}
