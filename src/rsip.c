/**************************************************************************** 
** File: rsip.c
**
** Author: Mike Borella
**
** Comments: Dump RSIP header information
**
*****************************************************************************/

#include <stdio.h>
#include "global.h"
#include "addrtoname.h"
#include "rsip.h"

/*
 * RSIP parameters
 */

#define RSIP_PARAM_ADDR_REQ    0
#define RSIP_PARAM_NUM_PORTS   1
#define RSIP_PARAM_ADDR        2
#define RSIP_PARAM_PORT_RANGE  3
#define RSIP_PARAM_LEASE_TIME  4
#define RSIP_PARAM_ERROR       5
#define RSIP_PARAM_CLIENT_ID   6
#define RSIP_PARAM_BIND_ID     7
#define RSIP_PARAM_MESSAGE_ID  8
#define RSIP_PARAM_TUNNEL_TYPE 9
#define RSIP_PARAM_METHOD      10
#define RSIP_PARAM_VENDOR      11

#define RSIP_MESSAGE_ERROR_RESP       1
#define RSIP_MESSAGE_REGISTER_REQ     2
#define RSIP_MESSAGE_REGISTER_RESP    3
#define RSIP_MESSAGE_DE_REGISTER_REQ  4
#define RSIP_MESSAGE_DE_REGISTER_RESP 5
#define RSIP_MESSAGE_ASSIGN_REQ_ADDR  6
#define RSIP_MESSAGE_ASSIGN_REQ_PORT  7
#define RSIP_MESSAGE_ASSIGN_REQ_EXT   8
#define RSIP_MESSAGE_ASSIGN_RESP_ADDR 9
#define RSIP_MESSAGE_ASSIGN_RESP_PORT 10
#define RSIP_MESSAGE_ASSIGN_RESP_EXT  11
#define RSIP_MESSAGE_FREE_REQ         12
#define RSIP_MESSAGE_FREE_RESP        13
#define RSIP_MESSAGE_QUERY_REQ        14
#define RSIP_MESSAGE_QUERY_RESP       15
#define RSIP_MESSAGE_DEALLOCATE       16
#define RSIP_MESSAGE_OK               17
#define RSIP_MESSAGE_LISTEN_REQ       18
#define RSIP_MESSAGE_LISTEN_RESP      19
#define RSIP_MESSAGE_LAST             20 /* not a real message, an EOL ptr */

#define PARAM_LEN 32

/*
 * Map parameters to strings
 */

char *rsip_param[] = 
{
  "address request",
  "number of ports",
  "address",
  "port range",
  "lease time",
  "error",
  "client ID",
  "bind ID",
  "message ID",
  "tunnel type",
  "method",
  "vendor specific"
};

/*
 * Map messages to string descriptions
 */

char *rsip_messages[] = 
{
  "",
  "error response",
  "register request",
  "register response",
  "deregister request",
  "deregister response",
  "assign request address",
  "assign request port",
  "assign request extension",
  "assign response address",
  "assign response port",
  "assign response extension",
  "free request",
  "free response",
  "query request",
  "query response",
  "deallocate",
  "ok",
  "listen request",
  "listen response"
};

/*----------------------------------------------------------------------------
**
** dump_rsip_param()
**
** Parse and dump an RSIP parameter
**
**----------------------------------------------------------------------------
*/

int dump_rsip_param(u_char *p)
{
  u_int16_t len;

  printf("Parameter:              %s\n", rsip_param[(int) *p]);
  memcpy((void *) &len, (void *) p+1, 2);
  len = ntohs(len);
  if (p + len + 3> p+10) /* fix me!!! */
    {
      printf("Bad parameter length");
      return -1;
    }
  printf("  Length:               %d\n", len);
  
  switch((int ) *p)
    {
    case RSIP_PARAM_CLIENT_ID:
      {
	u_int32_t clientid;

	memcpy((void *) &clientid, (void *) p+3, 4);
	printf("  Client ID:            %d\n", ntohl(clientid));
      }
      break;

    case RSIP_PARAM_MESSAGE_ID:
      {
	u_int32_t messageid;

	memcpy((void *) &messageid, (void *) p+3, 4);
	printf("  Message ID:           %d\n", ntohl(messageid));	
      }
      break;
    }

  return len + 3;
}

/*----------------------------------------------------------------------------
**
** dump_rsip()
**
** Parse RSIP packet and dump parameters
**
**----------------------------------------------------------------------------
*/

void dump_rsip(u_char *bp, int length)
{
  u_char *ep = bp + length;
  u_char *p;
  int message;
  int len;

  /*
   * Grab the message type
   */

  message = *bp;

  /*
   * Print it
   */

  printf("-----------------------------------------------------------------\n");
  printf("                        RSIP Packet\n");
  printf("-----------------------------------------------------------------\n");
  printf("Message type:           %s\n", rsip_messages[message]);

  /*
   * Print parameters
   */

  p = bp+1;
  while(p < ep)
    {
      len = dump_rsip_param(p);
      if (len == -1) break;
      p = p + len;
    }
}




