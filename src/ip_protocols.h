/**************************************************************************** 
** File: ip_protocols.h
**
** Author: Mike Borella
**
** Comments: IP next protocol numbers
**
** $Id: ip_protocols.h,v 1.11 2001/03/26 20:55:24 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef IP_PROTOCOLS_H
#define IP_PROTOCOLS_H

#include "ip.h"
#include "tcp.h"
#include "udp.h"
#include "ah.h"
#include "esp.h"
#include "icmp.h"
#include "igmp.h"
#include "ospf.h"
#include "icmpv6.h"
#include "ipv6.h"
#include "gre.h"
#include "rsvp.h"

/* 
 * This list contains lots of crufty old protocols just for kicks.
 * The complete list is at http://www.iana.org.
 */

#define PROTO_IPV6HOP    0
#define PROTO_ICMP       1
#define PROTO_IGMP       2
#define PROTO_GGP        3
#define PROTO_IPENCAP    4
#define PROTO_ST         5
#define PROTO_TCP        6
#define PROTO_CBT        7
#define PROTO_EGP        8
#define PROTO_IGP        9
#define PROTO_PUP        12
#define PROTO_UDP        17
#define PROTO_HMP        20
#define PROTO_XNSIDP     22
#define PROTO_RDP        27
#define PROTO_IPV6       41
#define PROTO_IPV6ROUTE  43
#define PROTO_IPV6FRAG   44
#define PROTO_IDRP       45
#define PROTO_RSVP       46
#define PROTO_GRE        47
#define PROTO_ESP        50
#define PROTO_AH         51
#define PROTO_NARP       54
#define PROTO_IPV6ICMP   58
#define PROTO_IPV6NONEXT 59
#define PROTO_IPV6OPTS   60
#define PROTO_RSPF       73
#define PROTO_VMTP       81
#define PROTO_OSPF       89
#define PROTO_IPIP       94
#define PROTO_ENCAP      98

void init_ip_protocols(void);

#endif

/* ip_protocols.h */
