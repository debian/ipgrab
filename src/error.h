/**************************************************************************** 
**
** File: error.h
**
** Author: Mike Borella
**
** Comments: Definitions for error.c.  The goal here is to have a clean
**           API for useful error reporting.  We three different types of
**           errors: messages fatal, and system.  
**
**           System errors result from failed system calls, and errno info
**           is reported.  The program is aborted
**
**           Fatal errors are any other error which requires program 
**           termination.
**
**           Message errors are like fatal errors, but just print a message
**           without aborting.
**
** $Id: error.h,v 1.3 2000/10/17 16:35:37 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef ERROR_H
#define ERROR_H

#include "global.h"

void error(int, char *, int, const char *fmt, va_list);
void error_system(char *fmt, ...);
void error_fatal(char *fmt, ...);
void error_message(char *fmt, ...);

#endif
